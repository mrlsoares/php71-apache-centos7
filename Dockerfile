FROM centos:centos7

MAINTAINER "Marcos Soares <mrlsoares@gmail.com>"

ENV container docker

RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
 && rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

# normal updates
RUN yum -y update

# php && httpd
RUN yum -y install mod_php71w php71w-opcache php71w-cli php71w-common php71w-gd php71w-intl php71w-mbstring php71w-mcrypt php71w-mysql php71w-mssql php71w-pdo php71w-pear php71w-soap php71w-xml php71w-xmlrpc httpd

# tools
RUN yum -y install epel-release iproute at curl crontabs git
RUN  yum install -y net-tools 
# pagespeed
RUN curl -O https://dl-ssl.google.com/dl/linux/direct/mod-pagespeed-stable_current_x86_64.rpm \
 && rpm -U mod-pagespeed-*.rpm \
 && yum clean all \
 && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
 && php composer-setup.php --install-dir=bin --filename=composer \
 && php -r "unlink('composer-setup.php');" \
 && rm -rf /etc/localtime \
 && ln -s /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

# we want some config changes
# COPY config/php_settings.ini /etc/php.d/
# COPY config/v-host.conf /etc/httpd/conf.d/

# create webserver-default directory




RUN systemctl enable httpd \
 && systemctl enable crond

RUN yum install -y git postfix sudo  cronie wget openssl-devel zip bzip2 net-tools mhash mc vim nano which ntp psmisc nodejs patch
RUN npm install -g grunt gulp gulp-cli requirejs 



RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer && \
    echo 'export PATH="/root/.composer/vendor/bin:$PATH"' >> /root/.bashrc


# Update Apache Configuration
RUN sed -E -i -e '/<Directory "\/var\/www\/html">/,/<\/Directory>/s/AllowOverride None/AllowOverride All/' /etc/httpd/conf/httpd.conf
RUN sed -E -i -e 's/DirectoryIndex (.*)$/DirectoryIndex index.php \1/g' /etc/httpd/conf/httpd.conf
WORKDIR /var/www/
EXPOSE 80 8000 443 9000 22 3000

# Start Apache
CMD ["/usr/sbin/httpd","-D","FOREGROUND"]